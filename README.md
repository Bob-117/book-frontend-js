# book-frontend-js

# VueJS Front app, Books CRUD (back-end NodeJS server at https://gitlab.com/Bob-117/book-backend-js )

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
